﻿using UnityEngine;
using System.Collections;

public class Anim : MonoBehaviour {

    void OnStateEnter(Animator anim)
    {

    }

    // Update is called once per frame
    void OnStateUpdate(Animator anim)
    {
 
    }


    public void wright(Animator anim) {
        anim.SetTrigger("wr");
    }
    public void wleft(Animator anim)
    {
        anim.SetTrigger("wl");
    }
    public void idler(Animator anim)
    {
        anim.SetTrigger("ir");
    }
    public void idlel(Animator anim)
    {   
        anim.SetTrigger("il");
    }

    void OnStateExit(Animator anim)
    {
        anim.ResetTrigger("il");
    }
}
