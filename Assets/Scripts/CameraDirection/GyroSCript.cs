﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GyroSCript : MonoBehaviour {
    GameObject camParent;
    GameObject text;

    public GameObject levelStart;
    public GameObject levelEnd;
    float min;
    float max;

    // Use this for initialization
    void Start () {
        camParent = new GameObject("CamParent");
        camParent.transform.position = this.transform.position;
        this.transform.parent = camParent.transform;
        Input.gyro.enabled = true;
        Debug.Log(Input.gyro.enabled);
       min = levelStart.transform.position.x  + 7.197966f;
       max = levelEnd.transform.position.x    -  7.197966f;



    }

    // Update is called once per frame  
    void Update () {
     
        this.transform.Translate(-Input.gyro.rotationRateUnbiased.y/15, 0, 0);
        gameObject.transform.position = (new Vector3(Mathf.Clamp(transform.position.x, min, max), gameObject.transform.position.y, gameObject.transform.position.z));


    }
}
