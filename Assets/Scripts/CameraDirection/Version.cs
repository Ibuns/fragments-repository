﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Version : MonoBehaviour {
    public GameObject Camera;

        // Use this for initialization
	void Start () {
        if (SystemInfo.supportsGyroscope == false)
        {
            Camera.gameObject.GetComponent<DragCamera>().enabled = true;
            Camera.gameObject.GetComponent<GyroSCript>().enabled = false;
        }
        else
        {
            Camera.gameObject.GetComponent<DragCamera>().enabled = false;
            Camera.gameObject.GetComponent<GyroSCript>().enabled = true;
        }
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
