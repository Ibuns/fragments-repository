﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HintButton : MonoBehaviour {

    public GameObject DialogText;
    public GameObject DialogBox;

    public void OnHintClick()
    {
        if (!DialogText.activeInHierarchy)
        {
            DialogText.SetActive(true);
            DialogBox.SetActive(true);
        }

    }

}
