﻿using UnityEngine;
using System.Collections;
//using UnityEngine.SceneManagement;
public class DoorSwitchScene : MonoBehaviour
{
    private bool _clickable = false;


    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.name == "Angelo")
        {
            _clickable = true;
           
            Debug.Log("kek123");
        }
    }


    void OnMouseDown()
    {
        if (!_clickable) return;
        SimpleSceneFader.ChangeSceneWithFade("Story2");
        PlayerPrefs.SetInt("Level2",1);

    }


    void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.name == "tyler" || other.gameObject.name =="Angelo" )
        {
            _clickable = false;
          
        }
    }
}