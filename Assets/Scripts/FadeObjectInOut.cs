﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class FadeObjectInOut : MonoBehaviour
{

    // publically editable speed

    public List<GameObject> CutScenes;
    public GameObject SkipButton;
    public GameObject NextButton;
    public float FadeDelay = 0.0f;
    public float FadeTime = 2f;
    public bool FadeInOnStart = false;
    public bool FadeOutOnStart = false;
    public bool CutScenesDone= false;
    private bool logInitialFadeSequence = false;
    private int currentCutScene;

    


    // store colours
    private Color[] colors;

    // allow automatic fading on the start of the scene
    IEnumerator Start()
    {
        currentCutScene = 0;
        //yield return null; 
        yield return new WaitForSeconds(FadeDelay);

        if (FadeInOnStart)
        {
            logInitialFadeSequence = true;
            FadeIn();
        }

        if (FadeOutOnStart)
        {
            FadeOut(FadeTime);
        }
    }




    // check the alpha value of most opaque object
    float MaxAlpha()
    {
        float maxAlpha = 0.0f;
        Renderer[] rendererObjects = GetComponentsInChildren<Renderer>();
        foreach (Renderer item in rendererObjects)
        {
            maxAlpha = Mathf.Max(maxAlpha, item.material.color.a);
        }
        return maxAlpha;
    }

    // fade sequence
    IEnumerator FadeSequence(float fadingOutTime)
    {
        // log fading direction, then precalculate fading speed as a multiplier
        bool fadingOut = (fadingOutTime < 0.0f);
        float fadingOutSpeed = 1.0f / fadingOutTime;

        // grab all child objects
        Renderer[] rendererObjects = GetComponentsInChildren<Renderer>();
        if (colors == null)
        {
            //create a cache of colors if necessary
            colors = new Color[rendererObjects.Length];

            // store the original colours for all child objects
            for (int i = 0; i < rendererObjects.Length; i++)
            {
                colors[i] = rendererObjects[i].material.color;
            }
        }

        // make all objects visible
        for (int i = 0; i < rendererObjects.Length; i++)
        {
            rendererObjects[i].enabled = true;
        }


        // get current max alpha
        float alphaValue = MaxAlpha();


        // This is a special case for objects that are set to fade in on start. 
        // it will treat them as alpha 0, despite them not being so. 
        if (logInitialFadeSequence && !fadingOut)
        {
            alphaValue = 0.0f;
            logInitialFadeSequence = false;
        }

        // iterate to change alpha value 
        while ((alphaValue >= 0.0f && fadingOut) || (alphaValue <= 1.0f && !fadingOut))
        {
            alphaValue += Time.deltaTime * fadingOutSpeed;

            for (int i = 0; i < rendererObjects.Length; i++)
            {
                Color newColor = (colors != null ? colors[i] : rendererObjects[i].material.color);
                newColor.a = Mathf.Min(newColor.a, alphaValue);
                newColor.a = Mathf.Clamp(newColor.a, 0.0f, 1.0f);
                rendererObjects[i].material.SetColor("_Color", newColor);
            }

            yield return null;
        }

        // turn objects off after fading out
        if (fadingOut)
        {
            for (int i = 0; i < rendererObjects.Length; i++)
            {
                rendererObjects[i].enabled = false;
            }
        }


        Debug.Log("fade sequence end : " + fadingOut);

    }


    public void FadeIn()
    {
        FadeIn(FadeTime);
    }

    public void FadeOut()
    {
        FadeOut(FadeTime);
    }

    void FadeIn(float newFadeTime)
    {
        StopAllCoroutines();
        StartCoroutine("FadeSequence", newFadeTime);
    }

    void FadeOut(float newFadeTime)
    {
        StopAllCoroutines();
        StartCoroutine("FadeSequence", -newFadeTime);
    }

    private void Update()
    {
        /*
#if UNITY_EDITOR
        if (Input.GetMouseButtonDown(0))
        {
            FadeOut();
            StartCoroutine("WaitForFourSeconds");
        }
#elif (UNITY_ANDROID || UNITY_IPHONE || UNITY_WP8)
        if ((Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began))
        {
            FadeOut();
            StartCoroutine("WaitForFourSeconds");
        }
#endif
        */
    }

    IEnumerator WaitForTwoSeconds(GameObject objectToDestroy)
    {
        yield return new WaitForSeconds(2);
        objectToDestroy.gameObject.SetActive(false);
    }
    
    public void FadeObject()
    {
        if(!(currentCutScene == CutScenes.Count-1))  //if not on last cutscene fadeout and increment
        {

            CutScenes[currentCutScene].gameObject.GetComponent<FadeObjectInOut>().FadeOut();
            StartCoroutine("WaitForTwoSeconds",CutScenes[currentCutScene].gameObject);
            currentCutScene++;
        }
        else if ((currentCutScene == CutScenes.Count-1))
        {
            Debug.Log("asdd");
            CutScenes[currentCutScene].gameObject.GetComponent<FadeObjectInOut>().FadeOut();
            StartCoroutine("WaitForTwoSeconds",CutScenes[currentCutScene].gameObject);
            DestroyObject(NextButton);
            DestroyObject(SkipButton);
            CutScenesDone = true;
        }
    }
    public void SkipCutScene()
    {

        for (int i = 1; i <CutScenes.Count;i++)
        {
            DestroyObject(CutScenes[i].gameObject);
        }
        CutScenes[0].GetComponent<FadeObjectInOut>().FadeOut();
        StartCoroutine("WaitForTwoSeconds",CutScenes[0].gameObject);
        DestroyObject(SkipButton);
        DestroyObject(NextButton);
        CutScenesDone = true;
    }
}