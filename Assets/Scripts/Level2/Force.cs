﻿using UnityEngine;
using System.Collections;

public class Force : MonoBehaviour {
    public float speed = 50f;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        float x = Input.acceleration.x;
        float y = Input.acceleration.y;
        this.GetComponent<Rigidbody2D>().AddForce(new Vector2(x*speed,y*speed));
    }
}
