﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MultiTouchTrash : MonoBehaviour {

    public LayerMask TouchInputMask;

    private List<GameObject> TouchList = new List<GameObject>();
    private GameObject[] TouchOld;
    private RaycastHit hit;
    private bool notEnd = true;
    private bool moving = false;
    public int count;
 //   public Text txt;
	// Use this for initialization
	void Start () {
      
	}
	
	// Update is called once per frame
	void Update () {

       // txt.text = "count " + count;
        /*   if (Input.touchCount > 0)
           {
               TouchOld = new GameObject[TouchList.Count];
               TouchList.CopyTo(TouchOld);
               TouchList.Clear();

               foreach (Touch touch in Input.touches)
               {

                   Ray ray = GetComponent<Camera>().ScreenPointToRay(touch.position);

                   if (Physics.Raycast(ray, out hit, TouchInputMask))
                   {
                       GameObject recipient = hit.transform.gameObject;
                       TouchList.Add(recipient);
                       txt.text = recipient.name;
                       if (touch.phase == TouchPhase.Began)
                       {
                           recipient.SendMessage("OnTouchDown", hit.point, SendMessageOptions.DontRequireReceiver);
                       }
                       if (touch.phase == TouchPhase.Stationary || touch.phase == TouchPhase.Moved)
                       {
                           recipient.SendMessage("OnTouchDown", hit.point, SendMessageOptions.DontRequireReceiver);
                       }
                       if (touch.phase == TouchPhase.Ended)
                       {
                           recipient.SendMessage("OnTouchUp", hit.point, SendMessageOptions.DontRequireReceiver);
                       }
                       if (touch.phase == TouchPhase.Canceled)
                       {
                           recipient.SendMessage("OnTouchExit", hit.point, SendMessageOptions.DontRequireReceiver);
                       }

                   }
               }

               foreach (GameObject g in TouchOld)
                   if (TouchList.Contains(g))
                   { g.SendMessage("OnTouchExit", hit.point, SendMessageOptions.DontRequireReceiver); }
           } */


        if (Input.touchCount > 0)
        {
            TouchOld = new GameObject[TouchList.Count];
            TouchList.CopyTo(TouchOld);
            TouchList.Clear();

            foreach (Touch touch in Input.touches)
            {

                Ray ray = GetComponent<Camera>().ScreenPointToRay(touch.position);

                if (Physics.Raycast(ray, out hit, TouchInputMask))
                {
                    GameObject recipient = hit.transform.gameObject;
                    TouchList.Add(recipient);
                   
                    if (touch.phase == TouchPhase.Began)
                    {
                      // recipient.SendMessage("OnTouchDown");
                      
                    }
                    if (touch.phase == TouchPhase.Stationary)
                    {
                        if(moving == false)
                        recipient.SendMessage("OnTouchDown");


                    }

                    if (touch.phase == TouchPhase.Moved && Input.GetTouch(0).deltaPosition.magnitude > 25f)
                    {
                        recipient.SendMessage("OnTouchUp");
                        moving = true;
                    }

                    if (touch.phase == TouchPhase.Canceled)
                    {
                        recipient.SendMessage("OnTouchUp");
                 
                    }
                    if (touch.phase == TouchPhase.Ended)
                    {
                        recipient.SendMessage("OnTouchUp");
                        moving = false;
                    }


                }
            }

         foreach (GameObject g in TouchOld)
                if (Input.touchCount >= 5 && TouchList.Contains(g) && g.GetComponent<TrashTouch>().Toucheds == true && notEnd)
                {
                    notEnd = false;
                    StartCoroutine(ChangeScene());
                }
        }
        if (count == 5)
        {       
            count = 30;
        }
    }


    IEnumerator ChangeScene()
    {
        yield return new WaitForSeconds(2f);
        PlayerPrefs.SetInt("Level5",1);
        SimpleSceneFader.ChangeSceneWithFade("Story5"); }
}
