﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TrashTouch : MonoBehaviour {
    public GameObject ObjectToMove;

    public bool Toucheds;
    private Vector2 oldP;
    private void Start()
    {
        oldP = ObjectToMove.transform.position;   
    }
    void OnTouchDown()
    {

        ObjectToMove.transform.position = this.transform.position;
        Toucheds = true;
    }

    void OnTouchUp()
    {
        ObjectToMove.transform.position = oldP;
        Toucheds = false;
    }

}
