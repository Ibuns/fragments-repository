﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ApplicationFocus : MonoBehaviour {

    public GameObject Asleep;

    bool isPaused = false;

    void OnApplicationFocus(bool hasFocus)
    {
        isPaused = !hasFocus;
    }

    void OnApplicationPause(bool pauseStatus)
    {
        isPaused = pauseStatus;
        if (isPaused == true)
        {
            Debug.Log("ad");
            isPaused = pauseStatus;
            PlayerPrefs.SetInt("Level7", 1);
            Asleep.gameObject.SetActive(true);
            StartCoroutine("WaitForFourSeconds");
            
        }
       
    }   
    IEnumerator WaitForFourSeconds()
    {
        Debug.Log("123asd");
        yield return new WaitForSeconds(2);
        Debug.Log("asd");
        SimpleSceneFader.ChangeSceneWithFade("Story7");
    }
}
