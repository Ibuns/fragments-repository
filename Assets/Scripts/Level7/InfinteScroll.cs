﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InfinteScroll : MonoBehaviour
{
    public float Location = 0;
    public float Speed;
    Vector3 startPOS;

    private void Start()
    {
        startPOS = transform.position;
    }

    private void Update()
    {
        transform.Translate((new Vector3(-1, 0, 0)) * Speed * Time.deltaTime);
        if(transform.position.x < Location)
        {
           transform.position = startPOS;
        }
    }


}
