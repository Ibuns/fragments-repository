﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelSelectScene : MonoBehaviour {
    public List<Button> Buttons;
	// Use this for initialization
    public void ResetB()
    {
        {
            PlayerPrefs.SetInt("Level1", 1);
            PlayerPrefs.SetInt("Level2", 0);
            PlayerPrefs.SetInt("Level3", 0);
            PlayerPrefs.SetInt("Level4", 0);
            PlayerPrefs.SetInt("Level5", 0);
            PlayerPrefs.SetInt("Level6", 0);
            PlayerPrefs.SetInt("Level7", 0);
            PlayerPrefs.SetInt("Level8", 0);
            PlayerPrefs.SetInt("Level9", 0);
            PlayerPrefs.SetInt("Level10", 0);

        }
    }
	void Start () {


        if (!PlayerPrefs.HasKey("Level1")) 
        {
            PlayerPrefs.SetInt("Level1", 1);
            PlayerPrefs.SetInt("Level2", 0);
            PlayerPrefs.SetInt("Level3", 0);
            PlayerPrefs.SetInt("Level4", 0);
            PlayerPrefs.SetInt("Level5", 0);
            PlayerPrefs.SetInt("Level6", 0);
            PlayerPrefs.SetInt("Level7", 0);
            PlayerPrefs.SetInt("Level8", 0);
            PlayerPrefs.SetInt("Level9", 0);
            PlayerPrefs.SetInt("Level10", 0);

        }
        

    }
	
	// Update is called once per frame
	void Update () {
      if (PlayerPrefs.GetInt("Level1")==0)
        {
            Buttons[0].interactable=false;
        }
      else
        {
            Buttons[0].interactable = true;
        }

        if (PlayerPrefs.GetInt("Level2") == 0)
        {
            Buttons[1].interactable = false;
        }
        else
        {
            Buttons[1].interactable = true;
        }

        if (PlayerPrefs.GetInt("Level3") == 0)
        {
            Buttons[2].interactable = false;
        }
        else
        {
            Buttons[2].interactable = true;
        }

        if (PlayerPrefs.GetInt("Level4") == 0)
        {
            Buttons[3].interactable = false;
        }
        else
        {
            Buttons[3].interactable = true;
        }

        if (PlayerPrefs.GetInt("Level5") == 0)
        {
            Buttons[4].interactable = false;
        }
        else
        {
            Buttons[4].interactable = true;
        }

        if (PlayerPrefs.GetInt("Level6") == 0)
        {
            Buttons[5].interactable = false;
        }
        else
        {
            Buttons[5].interactable = true;
        }

        if (PlayerPrefs.GetInt("Level7") == 0)
        {
            Buttons[6].interactable = false;
        }
        else
        {
            Buttons[6].interactable = true;
        }

        if (PlayerPrefs.GetInt("Level8") == 0)
        {
            Buttons[7].interactable = false;
        }
        else
        {
            Buttons[7].interactable = true;
        }

        if (PlayerPrefs.GetInt("Level9") == 0)
        {
            Buttons[8].interactable = false;
        }
        else
        {
            Buttons[8].interactable = true;
        }

        if (PlayerPrefs.GetInt("Level10") == 0)
        {
            Buttons[9].interactable = false;
        }
        else
        {
            Buttons[9].interactable = true;
        }
    }
}
