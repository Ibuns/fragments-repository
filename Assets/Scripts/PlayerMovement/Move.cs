﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Move : MonoBehaviour
{
    //flag to check if the user has tapped / clicked. 
    //Set to true on click. Reset to false on reaching destination
    public Animator anim;
    public bool DestinationReach;

    public bool idleP;  //if true left , if false right;
    public bool walkRight;
    public bool walkLeft;
    public bool flag = false;
    string currentAnim = null;
    //destination point
    private Vector2 endPoint;
    //alter this to change the speed of the movement of player / gameobject
   public float duration = 50.0f;
    //vertical position of the gameobject
    public LayerMask InputUI;
    [HideInInspector] public float yAxis;
    
    public virtual void Use( )
    {
        //check if the screen is touched / clicked   
        if ((Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began) || (Input.GetMouseButtonDown(0)))
        {
            //declare a variable of RaycastHit struct
            RaycastHit2D hit;
            //Create a Ray on the tapped / clicked position
            Ray2D ray;
            //for unity editor
#if UNITY_EDITOR
            Vector2 wp = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            //for touch device
#elif (UNITY_ANDROID || UNITY_IPHONE || UNITY_WP8)
            Vector2 wp = Camera.main.ScreenToWorldPoint(Input.GetTouch(0).position);

#endif
            ray = new Ray2D(wp, Vector2.zero);
            hit = Physics2D.Raycast(ray.origin, ray.direction, 1000,InputUI);

            if (hit.collider != null)
            {
                //set a flag to indicate to move the gameobject
                flag = true;
                //save the click / tap position
                endPoint = hit.point;
                //as we do not want to change the y axis value based on touch position, reset it to original y axis value
                endPoint.y = yAxis;

                if(endPoint.x < transform.position.x)
                {
                    walkLeft = true;
                    walkRight = false;   
                }
                else if (endPoint.x > transform.position.x)
                {
                    walkLeft = false ;
                    walkRight = true;
                }
              
            }
            //Check if the ray hits any collider
           /* if (Physics.Raycast(ray, out hit))
            {
                //set a flag to indicate to move the gameobject
                flag = true;
                //save the click / tap position
                endPoint = hit.point;
                //as we do not want to change the y axis value based on touch position, reset it to original y axis value
                endPoint.y = yAxis;
                Debug.Log(endPoint);
            }*/
            
        }
        //check if the flag for movement is true and the current gameobject position is not same as the clicked / tapped position
            if (flag && !Mathf.Approximately(gameObject.transform.position.magnitude, endPoint.magnitude))
            { //&& !(V3Equal(transform.position, endPoint))){
              //move the gameobject to the desired position

            if (flag && walkRight == false && walkLeft==true && !anim.GetCurrentAnimatorStateInfo(0).IsName("wr"))
                {
                    SetName("wl");
                    idleP = true; //idle position is left
                }
            else if (flag && walkLeft == false && walkRight == true&& !anim.GetCurrentAnimatorStateInfo(0).IsName("wl"))
                {
              
                SetName("wr");  
                idleP = false;//idle position is right
                }
                    gameObject.transform.position = Vector2.Lerp(gameObject.transform.position, endPoint, 1 / (duration * (Vector3.Distance(gameObject.transform.position, endPoint))));
            }
        //set the movement indicator flag to false if the endPoint and current gameobject position are equal
       
        DestinationReached();
    }
    public bool DestinationReached()
    {

        if (Mathf.Approximately(gameObject.transform.position.magnitude, endPoint.magnitude))
        {
            if (idleP)
            {
                SetName("il");
                //Debug.Log("pasok");
                walkLeft = false;
                walkRight = false;
            }
            else
            {
                SetName("ir");
                //Debug.Log("pasok2");
                walkLeft = false;
                walkRight = false;
            }
            if (flag)
            {
                
                flag = false;
                Debug.Log("I am here");
                DestinationReach = true;
                walkLeft = false;
                walkRight = false;
                return DestinationReach;


            }
            walkLeft = false;
            walkRight = false;
        }
           
        return DestinationReach;
    }
    // Use this for initialization
    void Start ()
    {
            
        //save the y axis value of gameobject
        yAxis = gameObject.transform.position.y;
    }
	
	// Update is called once per frame
	void Update () {
      
    }

    void SetName(string trigger)
    {
        if (!(currentAnim == trigger))
        {
            anim.SetTrigger(trigger);
        }
        currentAnim = trigger;
    }

}
