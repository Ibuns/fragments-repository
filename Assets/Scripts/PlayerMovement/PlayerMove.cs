﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;
//www.youtube.com/watch?v=gKjKFZ30684
public class PlayerMove : MonoBehaviour {

    public Text t;
	public float speed = 10, jumpvelocity=10;
    public bool isGrounded = false;
    public LayerMask playermask;
    bool walkingToRight;
    bool walkingToLeft;
    string currentAnim = null;
    private Transform myTrans, tagGround;
    private Rigidbody2D myBody;
	private float Hinput;

	// Use this for initialization
	void Start() {
		myBody = this.GetComponent<Rigidbody2D> ();
		myTrans = this.transform;
		tagGround = GameObject.Find (this.name + "/tag_ground").transform;	
	}
	// Update is called once per frame
	void FixedUpdate(){
		isGrounded = Physics2D.Linecast (myTrans.position, tagGround.position,playermask);
		//#if !UNITY_ANDROID && !UNITY_IPHONE && !UNITY_BLACKBERRY $$ !UNITY_WINRT
		move(Input.GetAxisRaw("Horizontal")); 
		//#endif
		move(Hinput);
	}
	public void move(float horizontalInput)
    {
            Vector2 moveVel = myBody.velocity;
            moveVel.x = horizontalInput * speed;
            myBody.velocity = moveVel;
	}
 
	public void startMoving(float HorizontalInput){

       
            Hinput = HorizontalInput;
    }

    public void wright(Animator anim)
    {
        SetName("wr", anim);

    }
    public void wleft(Animator anim)
    {
        SetName("wl", anim);

    }
    public void idler(Animator anim)
    {
        //anim.SetTrigger("ir");
        SetName("ir", anim);
    }
    public void idlel(Animator anim)
    {
        SetName("il", anim);
    }
     void OnStateUpdate(Animator anim)
    {
        if (isGrounded)
        {
            SetName("ir", anim);

        }
        else SetName("ir", anim); ;
    }

    void SetName(string trigger, Animator anim)
    {
        if(!(currentAnim == trigger))
        {
            anim.SetTrigger(trigger);
        }
        currentAnim = trigger;
    }

}	
