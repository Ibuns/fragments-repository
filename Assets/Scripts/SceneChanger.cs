﻿using UnityEngine;
using System.Collections;


public class SceneChanger : MonoBehaviour {

	// Use this for initialization
	void Start () {

    }

    // Update is called once per frame
    void Update () {
	    
	}

    public void Level1()
    {
        SimpleSceneFader.ChangeSceneWithFade("Story1");
    }

    public void Level2()
    {
        SimpleSceneFader.ChangeSceneWithFade("Story2");
    }

    public void Level3()
    {
        SimpleSceneFader.ChangeSceneWithFade("Story3");
    }
    public void Level4()
    {
        SimpleSceneFader.ChangeSceneWithFade("Story4");
    }
    public void Level5()
    {
        SimpleSceneFader.ChangeSceneWithFade("Story5");
    }

    public void Level6()
    {
        PlayerPrefs.SetInt("Level6",1);
        SimpleSceneFader.ChangeSceneWithFade("Story6");
    }
    public void Level7()
    {
        SimpleSceneFader.ChangeSceneWithFade("Story7");
    }
    public void Level8()
    {
        SimpleSceneFader.ChangeSceneWithFade("Story8");
    }
    public void Level9()
    {
        SimpleSceneFader.ChangeSceneWithFade("Story9");
    }
    public void Level10()
    {
        SimpleSceneFader.ChangeSceneWithFade("Story10");
    }
    public void Menu()
    {
        SimpleSceneFader.ChangeSceneWithFade("MenuScreen");
    }
    public void Select()
    {
        SimpleSceneFader.ChangeSceneWithFade("LevelSelect");
    }
    public void Options()
    {
        SimpleSceneFader.ChangeSceneWithFade("");
    }


}
