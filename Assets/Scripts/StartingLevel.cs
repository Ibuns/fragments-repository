﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartingLevel : MonoBehaviour {

	// Use this for initialization
	void Start () {
        if (PlayerPrefs.HasKey("Level1") == false)
        {
            PlayerPrefs.SetInt("Level1", 1);
            PlayerPrefs.SetInt("Level2", 0);
            PlayerPrefs.SetInt("Level3", 0);
            PlayerPrefs.SetInt("Level4", 0);
            PlayerPrefs.SetInt("Level5", 0);
            PlayerPrefs.SetInt("Level6", 0);
            PlayerPrefs.SetInt("Level7", 0);
            PlayerPrefs.SetInt("Level8", 0);
            PlayerPrefs.SetInt("Level9", 0);
            PlayerPrefs.SetInt("Level10", 0);

        }
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
