﻿using UnityEngine;
using System.Collections;

public class TimerFadeIn : MonoBehaviour {
	public float DiaTimer= 2f;
	public GameObject ObjectToFade;
    public GameObject FadeObjectInOutScript;

    private bool dt=false;
	// Use this for initialization
	void Start () {

	}

    // Update is called once per frame
    void Update() {
        if(FadeObjectInOutScript.GetComponent<FadeObjectInOut>().CutScenesDone==true)
        {
            dt = true;
        }
//#if UNITY_EDITOR
//        if (Input.GetMouseButtonDown(0))
//        {
//            dt = true;
//        }
//#elif (UNITY_ANDROID || UNITY_IPHONE || UNITY_WP8)
//        if ((Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began))
//        {
//              dt = true;
//        }
//#endif

        if (dt == true)
        {
            DiaTimer -= Time.deltaTime;
            if (DiaTimer <= 0)
            {
                ObjectToFade.gameObject.SetActive(true);
                dt = false;
                FadeObjectInOutScript.GetComponent<FadeObjectInOut>().CutScenesDone = false;
            }

        }
    }


}
