﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIButtons : MonoBehaviour {
    public GameObject PauseMenu;
  
	// Use this for initialization
	void Start () {
        
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void PauseButton()
    {
        PauseMenu.SetActive(true);
    }

    public void Closecanvas()
    {
        PauseMenu.SetActive(false);
    }

    public void MusicButton()
    {

        if (Music.EnableMusic == true)
        {
            Music.EnableMusic = false;
            Debug.Log("Kokoko");
        }
        else
        {
            
            Music.EnableMusic = true;
        }
    }

    public void LevelNine()
    {
        PlayerPrefs.SetInt("Level10",1);
        SimpleSceneFader.ChangeSceneWithFade("Story10");
    }

}
