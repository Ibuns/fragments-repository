﻿using UnityEngine;
using System.Collections;

public class doorlevel2 : MonoBehaviour 

	{

		public bool enter=false;

		private bool _clickable = false;

		void OnTriggerEnter2D(Collider2D other)
		{
			if (other.gameObject.name == "tyler")
			{
				_clickable = true;
				transform.GetChild(0).gameObject.SetActive(true);

			}
		}


		void OnMouseDown()
		{

			if (!_clickable) return;
			// float fadeTime = GameObject.Find("Main Camera").GetComponent<Fade>().BeginFade(1);
			// yield return new WaitForSeconds(fadeTime);

			SimpleSceneFader.ChangeSceneWithFade("level3");
		}


		void OnTriggerExit2D(Collider2D other)
		{
			if (other.gameObject.name == "tyler")
			{
				_clickable = false;
				transform.GetChild(0).gameObject.SetActive(false);
			}
		}
	}
